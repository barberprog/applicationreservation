# Application de réservation d'ateliers

## Description
Projet d'école d'une application pour des artisans de créer des ateliers et des participants de reserver des créneaux. 
 
##Langages utilisés

    HTML
    CSS
    Java
    SQL

## Frameworks

    Spring Boot
    Thymeleaf
    JPA (Java Persistence API)
    Hibernate
    Tomcat

## Installation
Cloner le repository:

    bash: git clone https://github.com/barberprog/applicationreservation.git

Naviger à la racine du fichier de lèapllication:

    bash: cd your-project

## Auteur
René Barber - barberprog@gmail.com

## Remerciements
Merci d'avoir regarder mon travail