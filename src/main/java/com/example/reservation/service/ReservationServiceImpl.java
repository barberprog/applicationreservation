package com.example.reservation.service;

import com.example.reservation.model.Atelier;
import com.example.reservation.model.Participant;
import com.example.reservation.model.Reservation;
import com.example.reservation.repository.ReservationRepository;

import jakarta.persistence.EntityNotFoundException;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
// Implémentation du service de réservation
public class ReservationServiceImpl implements ReservationService {

    private final ReservationRepository reservationRepository;
    private final AtelierService atelierService;
    private final ParticipantService participantService;

    @Autowired
    // Constructeur avec injection de dépendances
    public ReservationServiceImpl(ReservationRepository reservationRepository, AtelierService atelierService, ParticipantService participantService) {
        this.reservationRepository = reservationRepository;
        this.atelierService = atelierService;
        this.participantService = participantService;
    }
    
    @Override
    // Méthode pour sauvegarder une réservation
    public Reservation saveReservation(Reservation reservation) {
        return reservationRepository.save(reservation);
    }

    @Override
    // Méthode pour récupérer une réservation par son ID
    public Optional<Reservation> getReservationById(Long id) {
        return reservationRepository.findById(id);
    }

    @Override
    // Méthode pour récupérer toutes les réservations
    public List<Reservation> getAllReservations() {
        return reservationRepository.findAll();
    }

    @Override
    // Méthode pour mettre à jour une réservation
    public Reservation updateReservation(Reservation reservation) {
        return reservationRepository.save(reservation);
    }

    @Override
    // Méthode pour supprimer une réservation
    public void deleteReservation(Long id) {
        reservationRepository.deleteById(id);
    }
    
    @Transactional
    @Override
    // Méthode pour réserver un créneau
    public Reservation reserverCreneau(Long atelierId, Long participantId) {
        try {
            Optional<Atelier> optionalAtelier = atelierService.getAtelierById(atelierId);
            Optional<Participant> optionalParticipant = participantService.getParticipantById(participantId);
            
            Atelier atelier = optionalAtelier.orElseThrow(() -> new EntityNotFoundException("L'atelier n'existe pas."));
            Participant participant = optionalParticipant.orElseThrow(() -> new EntityNotFoundException("Le participant n'existe pas."));

            Reservation nouvelleReservation = new Reservation();
            nouvelleReservation.setAtelier(atelier);
            nouvelleReservation.setParticipant(participant);

            return reservationRepository.save(nouvelleReservation);
        } catch (EntityNotFoundException e) {
            throw e; 
        }
    }
    
    @Override
    // Méthode pour récupérer les réservations par l'ID du participant
    public List<Reservation> getReservationsByParticipant(Long participantId) {
        return reservationRepository.getReservationsByParticipantId(participantId);
    }

}