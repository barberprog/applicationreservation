package com.example.reservation.service;


import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import com.example.reservation.model.Atelier;

public interface AtelierService {
	Atelier saveAtelier(Atelier atelier);
	Optional<Atelier> getAtelierById(Long id);
	List<Atelier> getAllAteliers();
	Atelier updateAtelier(Atelier atelier);
	void deleteAtelier(Long id);
	Atelier creerNouvelAtelier(Long artisanId, String categorie, String description, LocalDateTime creneau);
}
