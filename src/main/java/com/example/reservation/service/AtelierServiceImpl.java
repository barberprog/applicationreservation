package com.example.reservation.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.reservation.model.Artisan;
import com.example.reservation.model.Atelier;
import com.example.reservation.repository.AtelierRepository;

import jakarta.persistence.EntityNotFoundException;
import jakarta.transaction.Transactional;

@Service
@Transactional
public class AtelierServiceImpl implements AtelierService {
	
	private final AtelierRepository atelierRepository;
	private final ArtisanService artisanService;

	@Autowired
	public AtelierServiceImpl(AtelierRepository atelierRepository, ArtisanService artisanService) {
	    this.atelierRepository = atelierRepository;
	    this.artisanService = artisanService;
	}

	@Override
	public Atelier saveAtelier(Atelier atelier) {
	    return atelierRepository.save(atelier);
	}

	@Override
	public Optional<Atelier> getAtelierById(Long id) {
	    return atelierRepository.findById(id);
	}

	@Override
	public List<Atelier> getAllAteliers() {
	    return atelierRepository.findAll();
	}

	@Override
	public Atelier updateAtelier(Atelier atelier) {
	    return atelierRepository.save(atelier);
	}

	@Override
	public void deleteAtelier(Long id) {
	    atelierRepository.deleteById(id);
	}
	
    @Transactional
    @Override
    public Atelier creerNouvelAtelier(Long artisanId, String categorie, String description, LocalDateTime creneau) {
        try {
            Optional<Artisan> optionalArtisan = artisanService.getArtisanById(artisanId);
            
            Artisan artisan = optionalArtisan.orElseThrow(() -> new EntityNotFoundException("L'artisan n'existe pas."));

            Atelier nouvelAtelier = new Atelier();
            nouvelAtelier.setArtisan(artisan);
            nouvelAtelier.setCategorie(categorie);
            nouvelAtelier.setDescription(description);
            nouvelAtelier.setCreneau(creneau);

            return atelierRepository.save(nouvelAtelier);
        } catch (EntityNotFoundException e) {
            throw e; 
        }
    }
	
}
