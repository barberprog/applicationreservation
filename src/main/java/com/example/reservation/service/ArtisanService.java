package com.example.reservation.service;

import com.example.reservation.model.Artisan;
import java.util.List;
import java.util.Optional;

public interface ArtisanService {
	Artisan saveArtisan(Artisan artisan);
	Optional<Artisan> getArtisanById(Long id);
	List<Artisan> getAllArtisans();
	Artisan updateArtisan(Artisan artisan);
	void deleteArtisan(Long id);
}
