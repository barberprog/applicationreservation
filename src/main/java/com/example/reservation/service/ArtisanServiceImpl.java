package com.example.reservation.service;

import com.example.reservation.model.Artisan;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.reservation.repository.ArtisanRepository;
import jakarta.transaction.Transactional;

@Service
@Transactional
public class ArtisanServiceImpl implements ArtisanService {

	private final ArtisanRepository artisanRepository;
	
	@Autowired
	public ArtisanServiceImpl(ArtisanRepository artisanRepository) {
		this.artisanRepository = artisanRepository;
	}
	
	@Override
	public Artisan saveArtisan(Artisan artisan) {
		return artisanRepository.save(artisan);
	}
	
	@Override
	public Optional<Artisan> getArtisanById(Long id) {
		return artisanRepository.findById(id);
	}
	
	@Override
	public List<Artisan> getAllArtisans() {
		return artisanRepository.findAll();
	}
	
	@Override
	public Artisan updateArtisan(Artisan artisan) {
		return artisanRepository.save(artisan);
	}
	
	@Override
	public void deleteArtisan(Long id) {
		artisanRepository.deleteById(id);
	}
}
