package com.example.reservation.service;

import com.example.reservation.model.Participant;
import java.util.List;
import java.util.Optional;

public interface ParticipantService {
	Participant saveParticipant(Participant participant);
	Optional<Participant> getParticipantById(Long id);
	List<Participant> getAllParticipants();
	Participant updateParticipant(Participant participant);
	void deleteParticipant(Long id);
}
