package com.example.reservation.service;

import com.example.reservation.model.Reservation;
import java.util.List;
import java.util.Optional;

public interface ReservationService {
	Reservation saveReservation(Reservation reservation);
	Optional<Reservation> getReservationById(Long id);
	List<Reservation> getAllReservations();
	Reservation updateReservation(Reservation reservation);
	void deleteReservation(Long id);
    Reservation reserverCreneau(Long atelierId, Long participantId);
    List<Reservation> getReservationsByParticipant(Long participantId);
}
