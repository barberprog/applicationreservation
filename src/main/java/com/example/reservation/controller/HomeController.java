package com.example.reservation.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {

    @GetMapping("/")
    // Gestionnaire de requête pour la page d'accueil
    public String home() {
        return "index";
    }
}