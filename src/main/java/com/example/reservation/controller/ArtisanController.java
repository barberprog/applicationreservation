package com.example.reservation.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.reservation.model.Artisan;
import com.example.reservation.service.ArtisanService;

@Controller
public class ArtisanController {

	private final ArtisanService artisanService;
	
    @Autowired
    // Injection de dépendance du service Artisan
    public ArtisanController(ArtisanService artisanService) {
    	this.artisanService = artisanService;
    }

    @GetMapping("/artisans")
    // Gestionnaire de requête pour afficher la liste des artisans
    public String listArtisans(Model model) {
        model.addAttribute("artisans", artisanService.getAllArtisans());
        return "artisans";
    }
    
    @PostMapping("/artisans")
    // Gestionnaire de requête pour créer un nouvel artisan
    public String createArtisan(@RequestParam String firstName, @RequestParam String lastName) {
        Artisan newArtisan = new Artisan(firstName, lastName);
        artisanService.saveArtisan(newArtisan);
        return "redirect:/artisans";
    }
    
    @PostMapping("/delete-artisan")
    // Gestionnaire de requête pour supprimer un artisan
    public String deleteArtisan(@RequestParam Long artisanId) {
        artisanService.deleteArtisan(artisanId);
        return "redirect:/artisans";
    }

}