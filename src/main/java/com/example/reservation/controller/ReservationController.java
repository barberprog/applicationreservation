package com.example.reservation.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.reservation.model.Atelier;
import com.example.reservation.model.Participant;
import com.example.reservation.model.Reservation;
import com.example.reservation.service.AtelierService;
import com.example.reservation.service.ParticipantService;
import com.example.reservation.service.ReservationService;

import jakarta.persistence.EntityNotFoundException;

@Controller
public class ReservationController {

    private final ReservationService reservationService;
    private final AtelierService atelierService;
    private final ParticipantService participantService;

    @Autowired
    // Contrôleur pour la gestion des réservations
    public ReservationController(ReservationService reservationService, AtelierService atelierService, ParticipantService participantService) {
        this.reservationService = reservationService;
        this.atelierService = atelierService;
        this.participantService = participantService;
    }
    
    @GetMapping("/reservations")
    // Méthode pour afficher la liste des réservations
    public String listReservations(@RequestParam(value = "participantId", required = false) Long participantId, Model model) {
        List<Reservation> reservations;
        if (participantId != null && participantId != 0) {
            reservations = reservationService.getReservationsByParticipant(participantId);
        } else {
            reservations = reservationService.getAllReservations();
        }
        // Toujours peupler la liste des participants
        List<Participant> participants = participantService.getAllParticipants();
        model.addAttribute("participants", participants);
        model.addAttribute("reservations", reservations);
        return "reservations";
    }

    @GetMapping("/reserverCreneau")
    // Méthode pour afficher le formulaire de réservation
    public String afficherFormulaireReservation(Model model) {
        List<Atelier> ateliers = atelierService.getAllAteliers();
        List<Participant> participants = participantService.getAllParticipants();
        model.addAttribute("ateliers", ateliers);
        model.addAttribute("participants", participants);
        return "reserverCreneau";
    }

    @PostMapping("/reservations")
    // Méthode pour réserver un créneau
    public String reserverCreneau(@RequestParam("atelierId") Long id, @RequestParam("participantId") Long participantId, Model model) {
        try {
            Reservation nouvelleReservation = reservationService.reserverCreneau(id, participantId);
            model.addAttribute("message", "Réservation réussie avec l'ID : " + nouvelleReservation.getId());
        } catch (EntityNotFoundException e) {
            model.addAttribute("erreur", "Erreur lors de la réservation : " + e.getMessage());
        }
        return "redirect:/reservations";
    }
    
    @PostMapping("/cancel-reservation")
    // Méthode pour annuler une réservation
    public String deleteReservation(@RequestParam Long reservationId) {
        reservationService.deleteReservation(reservationId);
        return "redirect:/reservations";
    }
}