package com.example.reservation.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.reservation.model.Participant;
import com.example.reservation.service.ParticipantService;

@Controller
public class ParticipantController {
	
	private final ParticipantService participantService;

    @Autowired
    // Contrôleur pour la gestion des participants
    public ParticipantController(ParticipantService participantService) {
    	this.participantService = participantService;
    }

    @GetMapping("/participants")
    // Méthode pour afficher la liste des participants
    public String listParticipants(Model model) {
        model.addAttribute("participants", participantService.getAllParticipants());
        return "participants"; 
    }
    
    @PostMapping("/participants")
    // Méthode pour ajouter un participant
    public String addParticipant(Participant participant) {
        participantService.saveParticipant(participant);
        return "redirect:/participants";
    }
    
    @PostMapping("/delete-participant")
    // Méthode pour supprimer un participant
    public String deleteParticipant(@RequestParam Long participantId) {
    	participantService.deleteParticipant(participantId);
        return "redirect:/participants";
    }
}