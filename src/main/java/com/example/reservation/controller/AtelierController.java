package com.example.reservation.controller;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.reservation.model.Artisan;
import com.example.reservation.model.Atelier;
import com.example.reservation.service.ArtisanService;
import com.example.reservation.service.AtelierService;

import jakarta.persistence.EntityNotFoundException; 

@Controller
public class AtelierController {

    private final AtelierService atelierService;
    private final ArtisanService artisanService;

    @Autowired
    // Contrôleur pour la gestion des ateliers
    public AtelierController(AtelierService atelierService, ArtisanService artisanService) {
        this.atelierService = atelierService;
        this.artisanService = artisanService;
    }

    @GetMapping("/ateliers")
    // Méthode pour afficher la liste des ateliers
    public String listAteliers(Model model) {
        model.addAttribute("ateliers", atelierService.getAllAteliers());
        return "ateliers";
    }
    
    @GetMapping("/creerAtelier")
    // Méthode pour afficher le formulaire de création d'atelier
    public String creerAtelier(Model model) {
        List<Artisan> artisans = artisanService.getAllArtisans();
        model.addAttribute("artisans", artisans);
        return "creerAtelier"; // Retourne le nom du modèle correct
    }
    
    @PostMapping("/creerAtelier")
    // Méthode pour créer un nouvel atelier
    public String creerNouvelAtelier(Model model, @RequestParam("artisanId") Long artisanId, 
                               @RequestParam("categorie") String categorie,
                               @RequestParam("description") String description,
                               @RequestParam("creneau") 
                               @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm") LocalDateTime creneau) {
        try {
            Atelier nouvelAtelier = atelierService.creerNouvelAtelier(artisanId, categorie, description, creneau);
            model.addAttribute("message", "Création d'atelier réussie avec l'ID : " + nouvelAtelier.getId());
        } catch (EntityNotFoundException e) {
            model.addAttribute("erreur", "Erreur de création d'atelier : " + e.getMessage());
        }
        return "redirect:/ateliers";
    }
    
    @PostMapping("/delete-atelier")
    // Méthode pour supprimer un atelier
    public String deleteAtelier(@RequestParam Long atelierId) {
        atelierService.deleteAtelier(atelierId);
        return "redirect:/ateliers";
    }
}