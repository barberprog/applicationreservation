package com.example.reservation.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.reservation.model.Atelier;

@Repository
public interface AtelierRepository extends JpaRepository<Atelier, Long> {
}

