package com.example.reservation.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.reservation.model.Reservation;

@Repository
public interface ReservationRepository extends JpaRepository<Reservation, Long> {
    // Méthode pour récupérer les réservations par l'ID du participant
    List<Reservation> getReservationsByParticipantId(Long participantId);
}