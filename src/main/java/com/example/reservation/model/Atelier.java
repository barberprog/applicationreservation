package com.example.reservation.model;

import java.time.LocalDateTime;
import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;

@Entity
@Table(name = "ateliers")
public class Atelier {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_artisan")
    private Artisan artisan;

    @Column(name = "categorie", nullable = false, length = 255)
    private String categorie;

    @Column(name = "description", columnDefinition = "TEXT")
    private String description; 
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "creneau")
    private LocalDateTime creneau;

    @OneToMany(mappedBy = "atelier", cascade = CascadeType.ALL)
    private List<Reservation> reservations;
       
	public Atelier() {
	}	

	public Atelier(Artisan artisan, String categorie, String description, LocalDateTime creneau) {
		this.artisan = artisan;
		this.categorie = categorie;
		this.description = description;
		this.creneau = creneau;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Artisan getArtisan() {
		return artisan;
	}

	public void setArtisan(Artisan artisan) {
		this.artisan = artisan;
	}

	public String getCategorie() {
		return categorie;
	}

	public void setCategorie(String name) {
		this.categorie = name;
	}

	public String getDescription() {
		return description;
	}	

	public LocalDateTime getCreneau() {
		return creneau;
	}

	public void setCreneau(LocalDateTime creneau) {
		this.creneau = creneau;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Reservation> getReservations() {
		return reservations;
	}

	public void setReservations(List<Reservation> reservations) {
		this.reservations = reservations;
	}

}
